<?php
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\UserController;

/*
|--------------------------------------------------------------------------
| API Backend Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API Backend routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('backend');
});

Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::group(['middleware' => ['role:admin|supervisor']], function () {
        Route::get('users', [UserController::class, 'index']);
    });

    Route::resource("products", ProductController::class);
    Route::resource("categories", CategoryController::class);
});

