<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = Role::all();
        if (count($roles) === 0) {
            Role::create(array('name' => 'admin', "guard_name" => "backend"));
            Role::create(array('name' => 'supervisor', "guard_name" => "backend"));
            Role::create(array('name' => 'seller', "guard_name" => "backend"));
            Role::create(array('name' => 'client', "guard_name" => "api"));
        }
    }
}
