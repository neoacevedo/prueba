<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Product::create(['id' => 1, 'name' => 'Camiseta', 'description' => 'Camiseta de algodón con logo', 'slug' => str('Camiseta de algodón con logo')->slug(), 'price' => 20.00, 'stock' => 10, 'image' => 'storage/product/image/camiseta.jpg', 'category_id' => 1]);
        Product::create(['id' => 2, 'name' => 'Saco de algodón', 'description' => 'Saco de algodón con logo', 'slug' => str('Saco de algodón con logo')->slug(), 'price' => 40.00, 'stock' => 5, 'image' => 'storage/product/image/saco-algodon.jpg', 'category_id' => 1]);
    }
}
