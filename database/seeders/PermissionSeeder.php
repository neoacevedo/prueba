<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            'Completo',
            'Registrar producto',
            'Registrar categoría',
            'Editar producto',
            'Editar categoría',
            'Borrar producto',
            'Borrar categoría',
            'Ver pedidos',
            'Crear pedido',
            'Registrar usuario',
            'Editar usuario',
            'Borrar usuario'
        ];
        $permission = Permission::all();
        if (count($permission) === 0) {
            foreach ($permissions as $permission) {
                Permission::create(['name' => $permission, 'guard_name' => 'backend']);
            }
        }

        $role = Role::where('name', 'admin')->first();
        $fullPermission = Permission::where('name', '=', 'Completo');
        $role->givePermissionTo($fullPermission);

        $supervisorRole = Role::where('name', '=', 'supervisor')->first();
        $supervisorRole->givePermissionTo([
            'Registrar producto',
            'Registrar categoría',
            'Editar producto',
            'Editar categoría',
            'Borrar producto',
            'Borrar categoría',
            'Registrar usuario',
            'Editar usuario'
        ]);

        $sellerRole = Role::where('name', '=', 'seller')->first();
        $sellerRole->givePermissionTo([
            'Registrar producto',
            'Ver pedidos',
            'Crear pedido'
        ]);
    }
}
