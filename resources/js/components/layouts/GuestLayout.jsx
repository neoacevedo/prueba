import React from 'react';
import { Navigate, NavLink, Outlet } from 'react-router-dom';
import * as Icon from 'react-bootstrap-icons';

export default function GuestLayout() {
    // const { user } = useAuth();

    // if user is logged in, redirect to profile page
    // if (user) {
    // 	return <Navigate to="/profile" />;
    // }
    return (
        <>
            <nav className="navbar navbar-expand-lg bg-body-tertiary">
                <div className="container-fluid">
                    <a href="#" className="navbar-brand">App</a>
                    <button
                        data-collapse-toggle="navbar-default"
                        type="button"
                        className="navbar-toggler"
                        aria-controls="navbar-default"
                        aria-expanded="false">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbar-default">
                        {/* <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className='nav-item'>
                                <NavLink
                                    to="/profile"
                                    className='nav-link'>
                                    Profile
                                </NavLink>
                            </li>
                            <li className='nav-item'>
                                <NavLink
                                    to="/about"
                                    className='nav-link'>
                                    About
                                </NavLink>
                            </li>
                        </ul> */}
                    </div>
                    <div className='d-flex'>
                        <NavLink
                            to="/cart"
                            className='btn btn-primary'>
                            <Icon.Cart />
                        </NavLink>
                    </div>
                </div>
            </nav>
            <main className="container flex justify-center flex-col items-center mt-10">
                <Outlet />
            </main>
        </>
    );
}