import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export default function Index() {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        fetchCategories()
    }, [])

    const fetchCategories = async () => {
        await axios.get(`http://localhost:8095/api/categories`).then(({ data }) => {
            if (data.success) {
                setCategories(data.categories)
            } else {
                Swal.fire({
                    text: data.message,
                    icon: "error"
                })
            }
        })
    }
    return (
        <>
            {
                categories.length > 0 && (
                    categories.map((category, key) => (
                        <div className='container mb-5' key={key}>
                            <div className='card' key={key}>
                                <div className='card-header'>
                                    <h5>{category.name}</h5>
                                </div>
                                <div className='card-body'>
                                    <div className='row'>
                                        {
                                            category.products && category.products.length ? (
                                                category.products.map((product, key) => (
                                                    <div className='col' key={key}>
                                                        <img className="img img-thumbnail" src={`${product.image}`} />
                                                        <h6 className='fs-4'>{product.name}</h6>
                                                        <p>$ {product.price}</p>
                                                        <Link to={`products/${product.slug}`} className='btn btn-success'>Ver producto</Link>
                                                    </div>
                                                ))
                                            ) : <div className='col'>
                                                <div className="alert alert-warning">
                                                    No hay productos en esta categoría
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                )
            }
        </>
    );
}