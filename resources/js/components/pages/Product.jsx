import React, { useEffect, useState } from 'react';
// import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Product() {

    const [product, setProduct] = useState([])
    const [category, setCategory] = useState([])

    const { slug } = useParams()

    useEffect(() => {
        fetchProducts()
    }, [])

    const fetchProducts = async () => {
        await axios.get(`http://localhost:8095/api/products/${slug}/`).then(({ data }) => {
            if (data.success) {
                setProduct(data.product)
                setCategory(data.product.category)
            } else {
                Swal.fire({
                    text: data.message,
                    icon: "error"
                })
            }
        }, [slug])
    }

    async function addToCart() {
        Swal.fire({
            text: 'Producto añadido al carro.',
            icon: "success"
        })
    }

    return (
        <div className="row">
            <div className="col-12">
                {
                    product ? (
                        <div className="card card-body">
                            <div className='card-header'>
                                <h2>{product.name}</h2>
                            </div>
                            <div className="card-body">

                                <div className="col">
                                    <img className="img img-thumbnail" src={`${product.image}`} />
                                    <h3>{product.name}</h3>
                                    <h4>Categoría: {category.name}</h4>
                                    <p>Precio: $ {product.price}</p>
                                    <button className="btn btn-outline-info mt-3" onClick={addToCart} type="button" id="button-addon1" >
                                        <i className="fas fa-cart-plus px-2"></i> Add To Cart
                                    </button>
                                </div>
                            </div>
                        </div>
                    ) : <div className="alert alert-warning">
                        El producto no se encuentra disponible.
                    </div>
                }
            </div>
        </div>
    )
}