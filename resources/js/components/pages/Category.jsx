import React, { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';
import axios from 'axios';

export default function Category() {

    const [products, setProducts] = useState([])

    useEffect(() => {
        fetchProducts()
    }, [])

    const fetchProducts = async () => {
        await axios.get(`http://localhost:8095/api/categories/ropa/`).then(({ data }) => {
            if (data.success) {
                setProducts(data.products)
            } else {
                Swal.fire({
                    text: data.message,
                    icon: "error"
                })
            }
        })
    }

    // const deleteProduct = async (id) => {
    //     const isConfirm = await Swal.fire({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //       }).then((result) => {
    //         return result.isConfirmed
    //       });

    //       if(!isConfirm){
    //         return;
    //       }

    //       await axios.delete(`http://localhost:8000/api/products/${id}`).then(({data})=>{
    //         Swal.fire({
    //             icon:"success",
    //             text:data.message
    //         })
    //         fetchProducts()
    //       }).catch(({response:{data}})=>{
    //         Swal.fire({
    //             text:data.message,
    //             icon:"error"
    //         })
    //       })
    // }

    return (
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="card card-body">
                        <div className="row">
                            {
                                products && products.length ? (
                                    products.map((row, key) => (
                                        <div className="col" key={key}>
                                            <img className="img img-thumbnail" src={`${row.image}`} />
                                            <h6>{row.name}</h6>
                                            <p>{row.price}</p>
                                        </div>
                                    ))
                                ) : <div className="alert alert-warning">
                                    No hay productos en esta categoría
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const rootElement = document.getElementById('category')
const root = createRoot(rootElement);

root.render(
    <React.StrictMode>
        <Category />
    </React.StrictMode>
);