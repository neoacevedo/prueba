import React from "react";
import { createBrowserRouter } from 'react-router-dom';
import Index from './pages/Index';
import Product from './pages/Product';
// import About from './pages/About';
// import Profile from './pages/Profile';
// import Register from './pages/Register';
// import ProtectedLayout from './components/ProtectedLayout';
import GuestLayout from './layouts/GuestLayout';

const router = createBrowserRouter([
    {
        path: '/',
        element: <GuestLayout />,
        children: [
            {
                path: '/',
                element: <Index />,
            },
            {
                path: '/products/:slug',
                element: <Product />
            }
        ],
    },
    // {
    // 	path: '/',
    // 	element: <ProtectedLayout />,
    // 	children: [
    // 		{
    // 			path: '/about',
    // 			element: <About />,
    // 		},
    // 		{
    // 			path: '/profile',
    // 			element: <Profile />,
    // 		},
    // 	],
    // },
]);

export default router;