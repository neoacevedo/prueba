<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Clase modelo para la tabla `products` 
 * 
 * @param int $id
 * @param string $name
 * @param string $slug
 * @param string $description
 * @param float $price
 * @param int $stock
 * @param string $image
 * @param int $category_id
 * @param string $created_at
 * @param string $updated_at
 */
class Product extends Model
{
    use HasFactory;

    /**
     * Obtener la categoría a la que pertenece el producto.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
