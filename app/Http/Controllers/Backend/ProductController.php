<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

/**
 * `ProductController` implementa todas las operaciones CRUD para la tabla products.
 */
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $products = Product::select(['id', 'name', 'description', 'image', 'stock', 'price'])->get();
        return ['success' => true, 'products' => $products];
    }

    /**
     * Store a newly created resource in storage.
     * @param \App\Http\Requests\StoreProductRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function store(StoreProductRequest $request)
    {

        try {

            Storage::disk('public')->putFileAs('product/image', $request->image, $request->image);
            Product::create($request->post() + ['image' => 'product/image/' . $request->image]);
            // Devolvemos la clave 'success' para no tener que entrar a validar el código de error en el frontend.
            // Basta con que esa clave esa true para que el mensaje sea correcto o false para indicar que hay un error.
            return [
                'success' => true,
                'message' => 'Product Created Successfully!!'
            ];
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'Something goes wrong while creating a product!!'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return ['success' => true, 'product' => $product];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            // Borrar la imagen actual
            if ($product->image) {
                $exists = Storage::disk('public')->exists("product/image/{$product->image}");
                if ($exists) {
                    Storage::disk('public')->delete("product/image/{$product->image}");
                }
            }

            // Almacenar la nueva imagen
            $imageName = $request->image->name . '.' . $request->image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('product/image', $request->image, $imageName);

            $product->image = $imageName;
            $product->save();

            return ['success' => true, 'message' => 'Producto actualizado correctamente!'];
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            return response()->json([
                'message' => 'No se pudo actualizar el producto!'
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        try {

            if ($product->image) {
                $exists = Storage::disk('public')->exists("product/image/{$product->image}");
                if ($exists) {
                    Storage::disk('public')->delete("product/image/{$product->image}");
                }
            }

            $product->delete();

            return [
                'success' => true,
                'message' => 'Producto borrado correctamente!'
            ];

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'El producto no pudo ser borrado!'
            ], 500);
        }
    }
}
