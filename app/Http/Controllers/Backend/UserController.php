<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:backend');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return ['success' => true, 'users' => $users];
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $token = auth('api')->attempt(['email' => $user->email, 'password' => $request->password]);
        return response()->json([
            'status' => 'success',
            'message' => 'Datos de usuario registrados correctamente!',
            'user' => $user,
            'authorisation' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        return ['success' => true, 'user' => $user];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        $updated = $user->update($request->all());
        return ['success' => $updated, 'user' => $user];
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        abort_unless(request()->user()->can('Borrar usuario'), 403, 'No tiene permitido ejecutar esta acción');

        try {
            $user->delete();

            return ['success' => true, 'message' => 'Datos de usuario borrados correctamente!'];
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            return response()->json([
                'success' => false,
                'message' => 'Los datos del usuario no pudieron ser borrados!'
            ], 500);
        }

    }
}
