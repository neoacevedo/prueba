<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Http\Controllers\Controller;

/**
 * `ProductController` implementa la parte del frontend para listar todos los productos o el producto junto a la categoría a la que pertenece.
 */
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $products = Product::select(['id', 'name', 'description', 'image', 'stock', 'price'])
            ->get();
        return ['success' => true, 'products' => $products];
    }

    /**
     * Muestra el detalle del producto.
     * @param string $slug El slug con el que es referenciado el producto en la URL. Ejemplo: `camiseta-con-logo`.
     * @return array|mixed|\Illuminate\Http\JsonResponse
     */
    public function show($slug)
    {
        $product = Product::where("slug", "=", $slug)->with('category')->first();

        if (!$product) {
            return response()->json(['success' => false, 'message' => 'Producto no encontrado'], 404);
        }
        return ['success' => true, 'product' => $product];
    }

}
