<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;

/**
 * `CategoryController` implementa la parte del frontend para listar categorías con productos o mostrar la categoría con sus productos.
 */
class CategoryController extends Controller
{
    /**
     * Lista todas las categorías con sus productos.
     * @return array
     */
    public function index()
    {
        $categories = Category::with("products")
            ->get();
        return ['success' => true, 'categories' => $categories];
    }

    /**
     * Muestra los productos que contiene la categoría.
     * @param string $slug El slug con el que es referenciada la categoría en la URL. Ejemplo: `accesorios-pc`.
     * @return array|mixed|\Illuminate\Http\JsonResponse
     */
    public function show($slug)
    {
        if ($slug !== null) {
            $category = Category::where('slug', '=', $slug)
                ->with('products')
                ->first();
        }
        if (!$category) {
            return response()->json(['success' => false, 'message' => 'Categoría no encontrada'], 404);
        }
        return ['success' => true, 'category' => $category];
    }

}
